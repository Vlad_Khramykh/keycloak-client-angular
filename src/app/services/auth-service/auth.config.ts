import {AuthConfig} from 'angular-oauth2-oidc';
import {environment} from '../../../environments/environment';


export const authConfig: AuthConfig = {

  issuer: environment.keycloak.issuer,
  redirectUri: environment.keycloak.redirectUri,
  clientId: environment.keycloak.clientId,
  responseType: environment.keycloak.responseType,
  scope: environment.keycloak.scope,
  requireHttps: environment.keycloak.requireHttps,
  showDebugInformation: environment.keycloak.showDebugInformation,
  disableAtHashCheck: environment.keycloak.disableAtHashCheck
};


export class OAuthModuleConfig {
  resourceServer: OAuthResourceServerConfig = {sendAccessToken: false};
}

export class OAuthResourceServerConfig {
  /**
   * Urls for which calls should be intercepted.
   * If there is an ResourceServerErrorHandler registered, it is used for them.
   * If sendAccessToken is set to true, the access_token is send to them too.
   */
  allowedUrls?: Array<string>;
  sendAccessToken = false;
  customUrlValidation?: (url: string) => boolean;
}
