import {APP_INITIALIZER, NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {AuthConfig, OAuthModule} from 'angular-oauth2-oidc';
import {AuthConfigService} from './services/auth-service/authconfig.service';
import {authConfig, OAuthModuleConfig} from './services/auth-service/auth.config';


export function init_app(authConfigService: AuthConfigService) {
  return () => authConfigService.initAuth();
}

@NgModule({
  imports: [HttpClientModule, OAuthModule.forRoot()],
  providers: [
    AuthConfigService,
    {provide: AuthConfig, useValue: authConfig},
    OAuthModuleConfig,
    {
      provide: APP_INITIALIZER,
      useFactory: init_app,
      deps: [AuthConfigService],
      multi: true
    }
  ]
})
export class AuthConfigModule {
}
