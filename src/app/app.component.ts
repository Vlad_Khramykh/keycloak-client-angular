import {Component, OnInit} from '@angular/core';
import jwt_decode from 'jwt-decode';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'angular-keycloak';
  token: string;
  tokenInfo;
  tokenWrap = '';

  constructor() {

  }

  ngOnInit(): void {
    this.token = localStorage.getItem('access_token');
    this.tokenInfo = this.getDecodedAccessToken(this.token);
    this.splitToken();
  }

  getDecodedAccessToken(token: string) {
    try {
      return jwt_decode(this.token);
    } catch (Error) {
      return null;
    }
  }


  splitToken() {
    let length = this.token.length;
    let partLength = 80;
    let countOfParts = (length / partLength) + 1;

    for (let partNum = 0; partNum <= countOfParts; partNum++) {
      this.tokenWrap += (this.token.substr(partNum * partLength, partLength) + '\n');
    }
  }
}

