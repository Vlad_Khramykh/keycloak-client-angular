export const environment = {
  production: false,
  envName: 'local',
  keycloak: {
    issuer: 'http://localhost:8080/auth/realms/is2',
    redirectUri: 'http://localhost:4200/',
    clientId: 'client1',
    responseType: 'code',
    scope: 'profile email',
    requireHttps: false,
    showDebugInformation: true,
    disableAtHashCheck: true
  }
};
